<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class BlogsController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(3);
        return view('blog.index')->with('posts', $posts);
    }

    public function create()
    {
        if(auth()->check()){
            return view('blog.create');
        } else {
            return view('auth.login');
        }
    }

    public function store(Request $request)
    {
        $image = null;
        if($request->has('file')){
            $file = $request->file;
            $image = time().'_'.$file->getClientOriginalName();
            $file->move(public_path('uploads'), $image);
        }

        Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'image' => $image,
            'user_id' => auth()->user()->id,
        ]);

        return redirect()->route('index')->with('success', 'Le post a bien été ajouté.');
    }

    public function show($id)
    {
        if(auth()->check()){
            $post = Post::findOrFail($id);
            return view('blog.show')->with('post', $post);
        } else {
            return view('auth.login');
        }
    }

    public function edit($id)
    {
        if(auth()->check()){
            $post = Post::findOrFail($id);
            return view('blog.edit')->with('post', $post);
        } else {
            return view('auth.login');
        }
    }

    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $image = $post->image;

        if($request->has('file')){
            $file = $request->file;
            $image = time().'_'.$file->getClientOriginalName();
            $file->move(public_path('uploads'), $image);
        }

        $post->update([
            'title' => $request->title,
            'content' => $request->content,
            'image' => $image,
            'user_id' => auth()->user()->id,
        ]);
        return redirect()->route('index')->with('success', 'Le post a bien été modifié.');
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return redirect()->route('index')->with('success', 'Le post a bien été supprimé.');
    }
}

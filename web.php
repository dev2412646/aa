<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogsController;


Route::get('/',[BlogsController::class,'index'])->name('index');
Route::get('/create',[BlogsController::class,'create'])->name('create');
Route::post('/store',[BlogsController::class,'store'])->name('store');
Route::delete('/delete{id}',[BlogsController::class,'destroy'])->name('delete');
Route::get('/edit{id}',[BlogsController::class,'edit'])->name('edit');
Route::put('/update{id}',[BlogsController::class,'update'])->name('update');
Route::get('/show{id}',[BlogsController::class,'show'])->name('show');
Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {return view('blog.templete');});
});

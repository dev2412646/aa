@extends('blog.templete')

@section('contenu')
<div class="container" style="margin-top: 3%">
    <div class="card">
        <img src="{{ asset('uploads/' . $post->image) }}" class="card-img-top" alt="Post Image">
        <div class="card-body">
            <h5 class="card-title">{{ $post->title }}</h5>
            <p class="card-text">{{ $post->content }}</p>
            <p class="card-text">Créé le : {{ $post->created_at }}</p>
            <p class="card-text">Publié par : {{ $post->user->name }}</p>
        </div>
    </div>
</div>
@endsection


@extends('blog.templete')

@section('contenu')
<div class="container">
    <h1 class="titre">Les posts</h1>
    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success')}}
    </div>
    @endif
    @if(session()->has('message'))
    <div class="alert alert-{{ session('alert-type') }}">
        {{ session()->get('message') }}
    </div>
    @endif
    <div class="card-container d-flex flex-wrap justify-content-between">
        @isset($posts)
        @foreach ($posts as $post)
        <div class="card mb-4" style="width: 30%;">
            <img src="{{ asset('./uploads/'.$post->image) }}" class="card-img-top" alt="" />
            <div class="card-body">
                <h5 class="card-title">{{ $post->title }}</h5>
                <p class="card-text">{{ $post->content }}</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Utilisateur: {{ $post->user->name }}</li>
                    <li class="list-group-item">Créé le: {{ $post->created_at->format('d/m/Y') }}</li>
                    @if($post->created_at != $post->updated_at)
                    <li class="list-group-item">Mis à jour le: {{ $post->updated_at->format('d/m/Y') }}</li>
                    @endif
                </ul>
                <div class="card-buttons">
                    <a href="{{ route('show', $post->id) }}" class="card-link btn btn-primary">More</a>
                    @auth
                    @if(auth()->user()->id == $post->user_id)
                    <form id={{$post->id}} action="{{ route('delete', $post->id) }}" method="post">
                        @csrf
                        @method('delete')
                        <button onclick="event.preventDefault(); if(confirm('Êtes-vous sûr de vouloir supprimer?')) document.getElementById({{$post->id}}).submit();" class="btn btn-danger ml-2" type="submit">Delete</button>
                    </form>
                    <a href="{{ route('edit', $post->id) }}" class="btn btn-warning ml-2">Edit</a>
                    @endif
                    @endauth
                </div>
            </div>
        </div>
        @endforeach
        @endisset
    </div>
    <div class="d-flex justify-content-center my-4">
        {{$posts->links()}}
    </div>
</div>
@endsection




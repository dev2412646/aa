<!DOCTYPE html>
<html lang="en">
<head>
    <title>blog</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="{{ route('index') }}">POSTS</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        @if(auth()->check())
        <li class="nav-item">
          <a class="nav-link" href="{{ route('create') }}">CREATE</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('profile.show') }}">{{ auth()->user()->name }}</a>
        </li>
        @else
        <li class="nav-item">
          <a class="nav-link" href="{{ route('register') }}">REGISTER</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('login') }}">LOG IN</a>
        </li>
        @endif
      </ul>
    </div>
  </div>
</nav>
</body>
</html>

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    // protected $table = 'blog'; // Correction de la faute de frappe dans le nom de la table

    protected $fillable = ['title', 'content', 'image', 'user_id']; // Ajout des champs manquants

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}


@extends('blog.templete')

@section('contenu')
<div class="container" style="margin-top: 8%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{route('store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="inputTitle">Title</label>
            <input type="text" class="form-control" name="title" id="inputTitle" placeholder="Title">
        </div>
        <div class="form-group">
            <label for="inputContent">Content</label>
            <textarea class="form-control" name="content" id="inputContent" rows="3" placeholder="Content"></textarea>
        </div>
        <div class="form-group">
            <label for="inputFile">File (Image)</label>
            <input type="file" class="form-control" name="file" id="inputFile" placeholder="Upload Image">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection



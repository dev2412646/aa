@extends('blog.templete')

@section('contenu')
<div class="container" style="margin-top: 8%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="{{ route('update', $post->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="inputTitle">Title</label>
            <input type="text" class="form-control" value="{{ $post->title }}" name="title" id="inputTitle" placeholder="Title">
        </div>
        <div class="form-group">
            <label for="inputContent">Content</label>
            <textarea class="form-control" name="content" id="inputContent" rows="5">{{ $post->content }}</textarea>
        </div>
        <div class="form-group">
            <label for="inputFile">Current Image</label>
            <br>
            <img src="{{ asset('uploads/' . $post->image) }}" alt="{{ $post->title }}" style="max-width: 200px;">
        </div>
        <div class="form-group">
            <label for="inputFile">New Image</label>
            <input type="file" class="form-control" name="file" id="inputFile" placeholder="Upload new image">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
</div>
@endsection


